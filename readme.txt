Very basic and small linux binary that simply takes up to 16 bytes, determined by a single hex character on the first command line argument, from stdin and prints only that to stdout.
Only 200 bytes, so should load quickly as well as be able to be put into tight script loops without much performance hit.
Usage:	ls | cutoff a
	ls | cutoff 4
Output: first 10 and 4 bytes of ls' output respectively
