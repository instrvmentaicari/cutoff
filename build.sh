make () {
 nasm ./cutoff.a -o ./cutoff
 chmod +x ./cutoff
}
debug () {
 lldb -s debug.lldb ./cutoff
}
install () {
 sudo cp ./cutoff /usr/bin/
}
uninstall () {
 sudo rm /usr/bin/cutoff 
}
make # call make by default
$1
